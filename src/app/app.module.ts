import { HttpInterceptService } from "./shared/services/http.intercept.service";
import {
  HttpClient,
  HTTP_INTERCEPTORS,
  HttpClientModule
} from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "@routes/app-routing.module";
import { AppComponent } from "./app.component";

import { LoginModule } from "./login/login.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { AboutComponent } from "./about/about.component";
import { HomeComponent } from "./home/home.component";

/* import {
  TranslateModule,
  TranslateLoader,
  TranslateService
} from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { LocalizeRouterService } from "localize-router";
 */
// AoT requires an exported function for factories
/* export function TanslateLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
} */

@NgModule({
  declarations: [AppComponent, AboutComponent, HomeComponent],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: "serverApp" }),

    AppRoutingModule,
    BrowserAnimationsModule,
    LoginModule,
    HttpClientModule
    /*     TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TanslateLoaderFactory,
        deps: [HttpClient]
      }
    }) */
  ],
  providers: [
    HttpClient,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptService,
      multi: true
    }

    /*     TranslateService,
    LocalizeRouterService */
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
