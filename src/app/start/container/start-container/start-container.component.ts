import { HttpClient } from "@angular/common/http";
import { CustomFormsModule } from "@forms/customForms.module";
import { FormsService } from "@forms/forms.services";
import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { map, tap } from "rxjs/operators";
@Component({
  selector: "app-start-container",
  templateUrl: "./start-container.component.html",
  styleUrls: ["./start-container.component.scss"],
  providers: [CustomFormsModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StartContainerComponent implements OnInit {
  constructor(private _http: HttpClient) {}

  ngOnInit() {}

  test() {
    this._http
      .post("https://hookia.herokuapp.com/login", "estttt")
      .pipe(
        tap((log: any) => {
          console.log("couuouo");
        })
      )
      .subscribe();
  }
}
