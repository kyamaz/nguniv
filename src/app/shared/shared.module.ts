import { CustomFormsModule } from "@forms/customForms.module";
import { NgModule } from "@angular/core";
/* import { TranslateModule } from "@ngx-translate/core";


 */
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { MatInputModule } from "@angular/material";
@NgModule({
  declarations: [],
  providers: [],
  bootstrap: [],
  imports: [
    CustomFormsModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    CommonModule
  ],
  exports: [
    CustomFormsModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    CommonModule
  ]
})
export class SharedModule {}
